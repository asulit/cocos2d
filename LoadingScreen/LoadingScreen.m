//
//  LoadingScreen.m
//
//  Created by Six Foot Three Foot on 28/05/12.
//  Copyright 2012 Six Foot Three Foot. All rights reserved.
//

#import "LoadingScreen.h"
#import "SimpleAudioEngine.h"

//The next scene you wish to transition to.
#import "HelloWorld.h"

@implementation LoadingScreen

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	
    NSString *className = NSStringFromClass([self class]); 
    // 'layer' is an autorelease object.
    id layer = [NSClassFromString(className) node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
    
    if ( ( self = [ super init] ) )
    {
        
        int fontScale = 2;
#ifdef __CC_PLATFORM_IOS
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            fontScale = 1;
#endif
        
        winSize = [[CCDirector sharedDirector] winSize];
        winCenter = ccp(winSize.width / 2, winSize.height / 2);
        //Load your assets (background, progress bars, etc)
        
        CCSprite *backDrop = [CCSprite spriteWithFile:@"Background_Striped.png"];
        [backDrop setColor:ccc3(50 + arc4random() % 200, 50 + arc4random() % 200, 50 + arc4random() % 200)];
        
        [backDrop setAnchorPoint:ccp(0, 0)];
        [self addChild:backDrop z:-10];
        
        progress = [CCProgressTimer progressWithSprite:[CCSprite spriteWithFile:@"progressbar.png"]];
        [progress setType:kCCProgressTimerTypeBar];
        [progress setMidpoint:ccp(0,0)];
        [progress setBarChangeRate:ccp(1,0)];
        [progress setPercentage:2.0f];
        [progress setPosition:winCenter];
        [self addChild:progress];
        
        CCLabelTTF *loadingString = [CCLabelTTF labelWithString:@"Loading" fontName:@"Verdana-Bold" fontSize:48 * fontScale];
        [loadingString setPosition:CGPointMake(self.centerPosition.x, self.centerPosition.y + (progress.contentSize.height * 2))];
        [self addChild:loadingString];
        
    }
    
    return self;
}

-(void) onEnterTransitionDidFinish
{
    NSString *path = [[CCFileUtils sharedFileUtils] fullPathFromRelativePath:@"preloadAssetManifest.plist"];
    
    NSDictionary *manifest = [NSDictionary dictionaryWithContentsOfFile:path];
    
    NSArray *spriteSheets   = [manifest objectForKey:@"SpriteSheets"];
    NSArray *images         = [manifest objectForKey:@"Images"];        
    NSArray *soundFX        = [manifest objectForKey:@"SoundFX"];
    NSArray *music          = [manifest objectForKey:@"Music"];
    NSArray *assets         = [manifest objectForKey:@"Assets"];
    NSNumber*customLoaders  = [manifest objectForKey:@"CustomLoaders"];
    
    assetCount = ([spriteSheets count] + [images count] + [soundFX count] + [music count] + [assets count] + [customLoaders boolValue]);
    progressInterval = 100.0 / (float) assetCount;
    
    NSMutableArray *loaderActions = [NSMutableArray array];
    
    if ([soundFX count])
    {
        CCCallFuncO *load = [CCCallFuncO actionWithTarget:self selector:@selector(loadSounds:) object:soundFX];
        [loaderActions addObject:load];
        
    }
    
    if ([spriteSheets count])
    {
        CCCallFuncO *load = [CCCallFuncO actionWithTarget:self selector:@selector(loadSpriteSheets:) object:spriteSheets];
        [loaderActions addObject:load];
        
    }
    
    if ([images count])
    {
        CCCallFuncO *load = [CCCallFuncO actionWithTarget:self selector:@selector(loadImages:) object:images];
        [loaderActions addObject:load];

    }
    
    if ([music count])
    {
        CCCallFuncO *load = [CCCallFuncO actionWithTarget:self selector:@selector(loadMusic:) object:music];
        [loaderActions addObject:load];

    }
    
    if ([assets count])
    {
        CCCallFuncO *load = [CCCallFuncO actionWithTarget:self selector:@selector(loadAssets:) object:assets];
        [loaderActions addObject:load];
    }
    
    if ([customLoaders boolValue])
    {
        CCCallFunc *load = [CCCallFunc actionWithTarget:self selector:@selector(customLoaders)];
        [loaderActions addObject:load];
    }
    
    [self runAction:[CCSequence actionWithArray:loaderActions]];
}

-(void) loadMusic:(NSArray *) musicFiles
{
    CCLOG(@"Start loading music");
    for (NSString *music in musicFiles)
    {
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:music];
        [self progressUpdate];
        
    }
}

-(void) loadSounds:(NSArray *) soundClips
{
    CCLOG(@"Start loading sounds");
    for (NSString *soundClip in soundClips)
    {
        [[SimpleAudioEngine sharedEngine] preloadEffect:soundClip];
        [self progressUpdate];
        
    }
    
}

-(void) loadSpriteSheets:(NSArray *) spriteSheets
{   
    for (NSString *spriteSheet in spriteSheets)
    {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spriteSheet];
        [self progressUpdate];
    }
}

-(void) loadImages:(NSArray *) images
{
    CCLOG(@"LoadingScreen - loadImages : You need to tell me what to do.");
    for (NSString *image in images)
    {
        //Do something with the images
        [self progressUpdate];        
    }
    
}

-(void) loadAssets:(NSArray *) assets
{
    //Overwrite me
    CCLOG(@"LoadingScreen - loadAssets : You need to tell me what to do.");
    for (NSString *asset in assets)
    {
        //Do something with the assets
        [self progressUpdate];        
    }
}

-(void) customLoaders
{
    CCLOG(@"LoadingScreen - Custom Loaders : You need to tell me what to do.");    
    
    [self progressUpdate];  

}

-(void) progressUpdate
{
    if (--assetCount)
    {
        [progress setPercentage:(100.0f - (progressInterval * assetCount))];
    }
    else {
        [progress setPercentage:100.0f];
        [self loadingComplete];
        CCLOG(@"All done loading assets.");
    }
    
}

-(void) loadingComplete
{
    CCDelayTime *delay = [CCDelayTime actionWithDuration:0.10f];
    CCCallBlock *swapScene = [CCCallBlock actionWithBlock:^(void) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0f scene:[SplashScreen scene]]];                
    }];
    
    CCSequence *seq = [CCSequence actions:delay, swapScene, nil];
    [self runAction:seq];
}

@end
