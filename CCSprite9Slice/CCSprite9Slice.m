/*
 * CCSprite9Slice.m
 *
 * Created by Six Foot Three Foot on 9/04/13.
 * Copyright (c) Six Foot Three Foot 2013. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


#import "CCSprite9Slice.h"

@interface CCSprite9Slice()
{
    CCSpriteBatchNode *contentSprite;
    CCSprite *slice_tr;
    CCSprite *slice_tc;
    CCSprite *slice_tl;
    CCSprite *slice_br;
    CCSprite *slice_bc;
    CCSprite *slice_bl;
    CCSprite *slice_lc;
    CCSprite *slice_rc;
    CCSprite *slice_center;
    CGSize    inset_;
    CGRect    boundingBox_;
    CGRect    texRect_;
}
@end

@implementation CCSprite9Slice

+(CCSprite9Slice *) spriteWithFile:(NSString *)file inset:(CGSize)inset
{
    CCSprite *sprite = [CCSprite spriteWithFile:file];
    if (sprite.texture)
        return [[[CCSprite9Slice alloc] initWithTexture:sprite.texture inset:inset texRect:sprite.textureRect] autorelease];
    else
        return nil;
}

+(CCSprite9Slice *) spriteWithSpriteFrameName:(NSString *)frameName inset:(CGSize)inset
{
    CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:frameName];

    
    
    if (sprite.texture)
        return [[[CCSprite9Slice alloc] initWithTexture:sprite.texture inset:inset texRect:sprite.textureRect] autorelease];
    else
        return nil;
}

-(id) initWithTexture:(CCTexture2D *)texture inset:(CGSize)inset texRect:(CGRect) texRect
{
    if ( ( self = [ super init] ))
    {
        contentSprite = [CCSpriteBatchNode batchNodeWithTexture:texture];
        boundingBox_  = texRect;
        texRect_ = texRect;
        
        //Get our bounding box.
        inset_ = inset;
        
        //This is the center of the sprite, this is the only part that actually scales.
        slice_center = [CCSprite spriteWithTexture:contentSprite.texture
                                              rect:CGRectInset(boundingBox_, inset.width, inset.height)];
        
        slice_tc    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x + inset.width,
                                                             texRect_.origin.y,
                                                             boundingBox_.size.width - (inset.width * 2),
                                                             inset.height)];
        
        [slice_tc setAnchorPoint:ccp(0.5, 0)];
        
        //This is our bottom Center slice
        slice_bc    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x + inset.width, texRect_.origin.y + (boundingBox_.size.height - inset.height),
                                                             boundingBox_.size.width - (inset.width * 2),
                                                             inset.height)];
        
        [slice_bc setAnchorPoint:ccp(0.5, 1)];
        
        
        //This is our Left Center slice
        slice_lc    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x, texRect_.origin.y + inset.height,
                                                             inset.width,
                                                             boundingBox_.size.height - (inset.height * 2))];
        
        [slice_lc setAnchorPoint:ccp(1, 0.5)];
        
        //This is our Right Center slice
        slice_rc    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x + (boundingBox_.size.width - inset.width),
                                                             texRect_.origin.y + inset.height,
                                                             inset.width,
                                                             boundingBox_.size.height - (inset.height * 2))];
        
        [slice_rc setAnchorPoint:ccp(0, 0.5)];
        
        //Four corners
        //This is our Top Left
        slice_tl    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x, texRect_.origin.y,
                                                             inset.width,
                                                             inset.height)];
        
        [slice_tl setAnchorPoint:ccp(1, 0)];
        
        //This is our Top Right
        slice_tr    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x + (boundingBox_.size.width - inset.width),
                                                             texRect_.origin.y,
                                                             inset.width,
                                                             inset.height)];
        
        [slice_tr setAnchorPoint:ccp(0, 0)];
        
        //This is our Bottom Left
        slice_bl    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x, texRect_.origin.y + (boundingBox_.size.height - inset.height),
                                                             inset.width,
                                                             inset.height)];
        
        [slice_bl setAnchorPoint:ccp(1, 1)];
        
        //This is our Bottom Right
        slice_br    = [CCSprite spriteWithTexture:contentSprite.texture
                                             rect:CGRectMake(texRect_.origin.x + (boundingBox_.size.width - inset.width),
                                                             texRect_.origin.y + (boundingBox_.size.height - inset.height),
                                                             inset.width,
                                                             inset.height)];
        
        [slice_br setAnchorPoint:ccp(0, 1)];
        
        [contentSprite addChild:slice_center];
        [contentSprite addChild:slice_tc];
        [contentSprite addChild:slice_bc];
        [contentSprite addChild:slice_lc];
        [contentSprite addChild:slice_rc];
        
        //Four Corners
        [contentSprite addChild:slice_tl];
        [contentSprite addChild:slice_tr];
        [contentSprite addChild:slice_bl];
        [contentSprite addChild:slice_br];
        
        [self setScale:1.0f];
        
        [self addChild:contentSprite];
    }
    
    return self;
}

-(void) setSize:(CGSize)size
{
    //Reset the scale
    [self setScale:1.0f];
    
    //We need to get the proportional scale
    CGSize scale = CGSizeMake((size.width -(inset_.width * 2)) / slice_center.contentSize.width,
                              (size.height -(inset_.height * 2)) / slice_center.contentSize.height);
    [self resizeToScale:scale];
    
    CCLOG(@"Done");
    
}

/*** This is a private method that actually does the scaling called from setScale and setSize */
-(void) resizeToScale:(CGSize) scale
{
    //    [self resizeToScale:scale];
    [slice_center setScaleX:scale.width];
    [slice_center setScaleY:scale.height];

    [slice_lc setScaleY:scale.height];
    [slice_lc setPosition:ccp(-slice_center.boundingBox.size.width / 2, 0)];
    
    [slice_rc setScaleY:scale.height];
    [slice_rc setPosition:ccp(slice_center.boundingBox.size.width / 2, 0)];    
    
    [slice_tc setScaleX:scale.width];
    [slice_tc setPosition:ccp(slice_bc.position.x, slice_center.boundingBox.size.height / 2)];
    
    [slice_bc setScaleX:scale.width];
    [slice_bc setPosition:ccp(slice_bc.position.x, -slice_center.boundingBox.size.height / 2)];
    
    [slice_tl setPosition:ccp(-slice_center.boundingBox.size.width / 2, slice_center.boundingBox.size.height / 2)];
    [slice_tr setPosition:ccp(slice_center.boundingBox.size.width / 2, slice_center.boundingBox.size.height / 2)];
    [slice_br setPosition:ccp(slice_center.boundingBox.size.width / 2, -slice_center.boundingBox.size.height / 2)];
    [slice_bl setPosition:ccp(-slice_center.boundingBox.size.width / 2, -slice_center.boundingBox.size.height / 2)];
   
    boundingBox_ = CGRectInset(slice_center.boundingBox, -inset_.width, -inset_.height);
}

-(void) setScale:(float)s
{
    //We need to reset the scale in case it's been changed already
    if (s != 1)
    {
        if (slice_center.scale != 1)
            [self setScale:1];
    }
    
    //We need to get the full scale ratio minus the insets
    CGSize targetSize = CGSizeMake(texRect_.size.width * s, texRect_.size.height * s);
    CGSize scale = CGSizeMake((targetSize.width - (inset_.width * 2)) / slice_center.contentSize.width,
                                    (targetSize.height - (inset_.height * 2)) / slice_center.contentSize.height);
    
    [self resizeToScale:scale];

}

/*** Set the color by recursing through children */
-(void) setColor:(ccColor3B)color
{
	CCSprite *item;
	CCARRAY_FOREACH(contentSprite.children, item) {
		[item setColor:color];
	}
}

/*** Set the opacity by recursing through children */
-(void) setOpacity:(GLubyte)opacity
{
	CCSprite *item;
	CCARRAY_FOREACH(contentSprite.children, item) {
		[item setOpacity:opacity];
	}
}

-(CGRect) boundingBox
{
    //This will need to scale
    return boundingBox_;
}


@end